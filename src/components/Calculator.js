import "../App.css"
import React from "react"

function reduce(a,b) {
    if (b === 0){
      return a;
    }
    else{
      return reduce(b, a % b)
    }
}

class Calculator extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          resX: 0,
          resY: 0,
          aspectFraction: ''
        };
        this.onClick = this.onClick.bind(this)
    }

    onClick() {
        var resY = this.state.resY;
        var resX = this.state.resX;

        var divisor = reduce(resX, resY);
      
        var fractionTop = resX / divisor;
        var fractionBot = resY / divisor;
        this.setState({aspectFraction: fractionTop + "/" + fractionBot});

        console.log(this.state.aspectFraction);

    }

    updateResX(evt) {
        this.setState({
            resX: evt.target.value
        });
    }

    updateResY(evt) {
        this.setState({
            resY: evt.target.value
        });
    }

    render(){
        return (
            <div>
                <input type="text" 
                        name="valX" 
                        onChange={evt => this.updateResX(evt)} 
                        placeholder="width"/>
                <input type="text" 
                        name="valY"
                        onChange={evt => this.updateResY(evt)} 
                        placeholder="Height"/>
                <button onClick={this.onClick} className="btn">Calculate</button>
                <h2>Aspect ratio: {this.state.aspectFraction}</h2>
            </div>
        );
    }

};

export default Calculator
