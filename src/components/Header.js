import "../App.css"

const Header = () => {
    return (
        <header className="header">
            <h1>Aspect ratio calculator</h1>
        </header>
    )
}

export default Header
